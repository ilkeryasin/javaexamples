package com.controller;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.model.SelectItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.model.Message;
import com.service.MessageService;
import com.util.Branch;
import com.util.Language;

--my project
@ManagedBean(name="messageConroller")
public class MessageController {

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

	private List<Message> messageList;
	
	@ManagedProperty(value="#{messageService}")
	private MessageService messageService;
	
	private Message message;
	
	private String selectedBranch;
    private List<SelectItem> branchList;
    private String selectedLang;
    private List<SelectItem> langList;
	
	@PostConstruct
	public void onLoad(){
		this.messageList = messageService.listMessages();
		fillBranch();
		fillLanguage();
	}
	
	private void fillBranch(){
		if(this.branchList == null){
			this.branchList = new ArrayList<>();
		}
		this.branchList.add(new SelectItem(null,"L�tfen Se�iniz"));
		this.branchList.add(new SelectItem(Branch.INTERNET,Branch.INTERNET.getName()));
		this.branchList.add(new SelectItem(Branch.MOBILE,Branch.MOBILE.name()));
	}
	
	private void fillLanguage(){
		if(this.langList == null){
			this.langList = new ArrayList<>();
		}
		this.langList.add(new SelectItem("L�tfen Se�iniz"));
		this.langList.add(new SelectItem(Language.TR.getName()));
		this.langList.add(new SelectItem(Language.EN.getName()));
	}
	
	public void saveMessage(){
		try {
			messageService.createMessage(message);
		} catch (Exception e) {
			logger.error("saveMessage error : ", e);
		}
	}

	public List<Message> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}

	public MessageService getMessageService() {
		return messageService;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public List<SelectItem> getBranchList() {
		return branchList;
	}

	public void setBranchList(List<SelectItem> branchList) {
		this.branchList = branchList;
	}

	public List<SelectItem> getLangList() {
		return langList;
	}

	public void setLangList(List<SelectItem> langList) {
		this.langList = langList;
	}
	
	
	
}

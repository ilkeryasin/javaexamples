package com.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.MessageDAO;
import com.model.Message;

@Service
@SessionScoped
public class MessageServiceImpl implements MessageService{
	
	private MessageDAO messageDAO;
	 
    public void setMessageDAO(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    @Transactional
	public void createMessage(Message m) {
		this.messageDAO.createMessage(m);
		
	}

    @Transactional
	public List<Message> listMessages() {
		return this.messageDAO.listMessages();
	}
	
	

}

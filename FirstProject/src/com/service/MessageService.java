package com.service;

import java.util.List;
import com.model.Message;


public interface MessageService {

	public void createMessage(Message m);
	public List<Message> listMessages();
	
}

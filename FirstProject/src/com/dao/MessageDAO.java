package com.dao;

import java.util.List;

import com.model.Message;

public interface MessageDAO {
	
	public void createMessage(Message m);
	public List<Message> listMessages();

}

package com.dao;


import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.model.Message;

@Repository
public class MessageDAOImpl implements MessageDAO{

	private static final Logger logger = LoggerFactory.getLogger(MessageDAOImpl.class);
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public void createMessage(Message m) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(m);
		logger.info("Message saved successfully, Message Details=" + m);
	}

	@SuppressWarnings("unchecked")
	public List<Message> listMessages() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Message> messagesList = session.createQuery("from Message").list();
		for (Message m : messagesList) {
			logger.info("Message List::" + m);
		}
		return messagesList;
	}

}

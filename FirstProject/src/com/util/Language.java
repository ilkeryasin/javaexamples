package com.util;

public enum Language {

	TR("T�rk�e"),
	EN("English");
	
	private String name;

	private Language(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
}

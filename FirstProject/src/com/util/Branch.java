package com.util;

public enum Branch {
	
	MOBILE("Mobil"),
	INTERNET("Ưnternet");
	
	private String name;

	private Branch(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}

}

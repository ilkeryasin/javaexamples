package main;
import entity.ParameterDTO;
import enums.State;
import service.Operation;
import util.OperationFactory;

public class Test {
	public static void main(String[] args) throws Exception { 
		ParameterDTO dto = new ParameterDTO();
		
		dto.setState(State.INSERT); 
		dto.setParameterKey("Key");
		dto.setParameterValue("Value"); 
		dto.setId(1L);
		
		Operation insert = OperationFactory.getInstance(dto); 
		insert.makeOperation(dto); 
		dto.setState(State.UPDATE); 
		dto.setParameterValue("Value2"); 
		
		Operation update = OperationFactory.getInstance(dto);
		update.makeOperation(dto); 
		dto.setState(State.DELETE);
		
		Operation delete = OperationFactory.getInstance(dto); 
		delete.makeOperation(dto); 
		
	}
}

package service;
import entity.ParameterDTO;

public interface Operation {
	public void doControl(); 
	public void makeOperation(ParameterDTO dto); 
}

package service;
import dao.PersistenceImpl;
import entity.ParameterDTO;
import enums.State;
import logs.Logger;

public class OperationImpl implements Operation{

	private ParameterDTO pDto;
	private ParameterDTO data;
	private boolean resultState = false;
	private PersistenceImpl persistenceImpl = new PersistenceImpl();
	
	@Override
	public void doControl() {
		if(this.pDto.getState() == State.INSERT){
			data = persistenceImpl.findById(this.pDto.getId());
			if(data == null){
				resultState = true; 
				Logger.log("Insert doControl() success! ", this.pDto);
			}else {
				resultState = false;
				Logger.log("Insert doControl() not success! ", this.pDto);
			}
		}
		
		if(this.pDto.getState() == State.UPDATE){
			data = persistenceImpl.findById(this.pDto.getId());
			if(data != null){
				resultState = true;
				Logger.log("Update doControl() success! ", this.pDto); 
			}else {
				resultState = false;
				Logger.log("Update doControl() not success! ", this.pDto);
			}
		}
		
		if(this.pDto.getState() == State.DELETE){
			data = persistenceImpl.findById(this.pDto.getId());
			if(data != null){
				resultState = true; 
				Logger.log("Delete doControl() success! ", this.pDto);
			}else {
				resultState = false;
				Logger.log("Delete doControl() not success! ", this.pDto);
			}
		}
		
		
	}

	public void makeOperation(ParameterDTO dto) {
		this.pDto = dto;
		
		doControl();
		
		if(resultState){
			if(dto.getState() == State.INSERT){
				persistenceImpl.insert(this.pDto);
			}
			if(dto.getState() == State.UPDATE){
				persistenceImpl.update(this.pDto);
			}
			if(dto.getState() == State.DELETE){
				persistenceImpl.delete(this.pDto);
			}
		}
		this.resultState = false;
	}
	
}

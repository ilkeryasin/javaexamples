package util;
import entity.ParameterDTO;
import service.Operation;

public class OperationFactory {
	public static Operation getInstance(ParameterDTO dto) {
		if(dto == null){
			dto = new ParameterDTO();
		} 
		return (Operation) dto; 
	}
}

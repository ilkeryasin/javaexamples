package dao;
import java.util.HashMap;
import java.util.Map;

import entity.ParameterDTO;
import logs.Logger;

public class PersistenceImpl implements Persistence{
	Map<Long,ParameterDTO> parameters = new HashMap<Long,ParameterDTO>(); 
	
	public void insert(ParameterDTO obj){ 
		try {
			parameters.put(obj.getId(), obj);
			Logger.log("Insert makeOperation Success : ", obj);
		} catch (Exception e) {
			Logger.log("Insert makeOperation Error : " + e.getMessage(), obj);
		}
	} 
	
	public void update(ParameterDTO obj){
		try {
			parameters.put(obj.getId(), obj);
			Logger.log("Update makeOperation Success : ", obj);
		} catch (Exception e) {
			Logger.log("Update makeOperation Error : " + e.getMessage(), obj);
		}
	} 
	
	public void delete(ParameterDTO obj){ 
		try {
			parameters.remove(obj.getId());
			Logger.log("Delete makeOperation Success : ", obj);
		} catch (Exception e) {
			Logger.log("Delete makeOperation Error : " + e.getMessage(), obj);
		}
	} 
	
	public ParameterDTO findById(long id){
		return parameters.get(id); 
	}
	
	
}

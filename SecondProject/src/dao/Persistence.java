package dao;

import entity.ParameterDTO;

public interface Persistence {
	
	public void insert(ParameterDTO obj);
	
	public void update(ParameterDTO obj);
	
	public void delete(ParameterDTO obj);
	
	public ParameterDTO findById(long id);
	
}

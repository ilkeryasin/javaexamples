import java.util.ArrayList;
import java.util.List;

public class Sorting {

	public static List<String> sortWords(List<String> words) {

        List<String> sortedList = new ArrayList<String>();
        while (!words.isEmpty()) {
            String bigger = "";
            int biggerCount = 0;

            for (String word : words) {
                int count = word.length() - word.replace("a", "").length();
                if (count > biggerCount) {
                    bigger = word;
                    biggerCount = count;
                }
                if (count == biggerCount) {
                    if (word.length() > bigger.length()) {
                        bigger = word;
                        biggerCount = count;
                    }
                }
            }

            sortedList.add(bigger);
            words.remove(bigger);

        }
        return sortedList;
    }

}

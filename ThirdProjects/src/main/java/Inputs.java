import java.util.ArrayList;
import java.util.List;

public class Inputs {

	public static void main(String[] args){
    	
        List<String> inputs = new ArrayList<String>();
        
        inputs.add("aaaasd");
        inputs.add("a");
        inputs.add("aab");
        inputs.add("aaabcd");
        inputs.add("ef");
        inputs.add("cssssssd");
        inputs.add("fdz");
        inputs.add("kf");
        inputs.add("zc");
        inputs.add("lklklklklklklklkl");
        inputs.add("l");
 
        System.out.println(Sorting.sortWords(inputs));
    }
}

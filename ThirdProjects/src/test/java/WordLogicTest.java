import java.util.ArrayList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class WordLogicTest extends TestCase {
    
    public WordLogicTest(String testName){
        super( testName );
    }

    public static Test suite(){
       return new TestSuite(WordLogicTest.class);
    }

    public void testApp(){
        List<String> testList = new ArrayList<String>();
        List<String> returnedList = new ArrayList<String>();
        
        testList.add("dddd");
        testList.add("aasd");
        testList.add("aaas");
        testList.add("assd");
        
        
        List<String> expectedList = new ArrayList<String>();
        expectedList.add("aaas");
        expectedList.add("aasd");
        expectedList.add("assd");
        expectedList.add("dddd");
        
        returnedList = Sorting.sortWords(testList);
     
        assertEquals(expectedList, returnedList);
    }
}
